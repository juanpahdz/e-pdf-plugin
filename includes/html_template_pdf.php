<?php 
function createEPDF($id) {
    if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
        // Put your plugin code here

            $mpdf = new \Mpdf\Mpdf();

            $id = str_replace(' ', '', $id);
            $product = wc_get_product($id);
            $items = $product->get_attributes();
                ob_start();
            ?>
            <style>
                .epdf_templ_header{
                    font-family: 'Arial';
                    width: 100%;
                    padding: 20px;
                    height: 80px;
                }

                .epdf_templ_header_info{
                    width:70%;
                    float:left;
                    display: block;
                }

                .epdf_templ_header_info h2{
                    font-size: 22px;
                    color: #C0022B;
                    margin:0;
                }

                .epdf_templ_header_info span{
                    font-size:15px;
                    font-weight: 400;
                }

                .epdf_templ_header_info a{
                    text-decoration: none;
                    color:black;
                }

                .epdf_templ_header_info{
                    font-size:15px;
                }
                .pdfe_templ_phone_number{
                    width:30%;
                    display: block;
                    float: right;
                    text-align: center;
                    background-color: #C0022B;
                }

                .pdfe_templ_phone_number a{
                    text-decoration:none;
                    color: white;
                    font-weight: 600;
                    margin: 0;
                    font-size:25px
                }

                .epdf_templ_image_block{
                    display: block;
                    width: 30%;
                    float: left;
                    text-align: center;
                    height: 115px;
                }

                .epdf_templ_image_block img{
                    width: 150px;
                }

                .epdf_templ_main_info{
                    height: 90px;
                    padding-left: 25px;
                    float: left;
                    font-family: Arial;
                    padding-top: 25px;
                }

                .epdf_templ_main_info h1{
                    margin: 0;
                    font-size:20px
                }

                .epdf_container{
                    margin: auto;
                    width: 100%;
                    height: 95px;
                }

                .epdf_templ_container{
                    margin: auto;
                    width: 95%;
                    display: block;
                }
                
                .epdf_th{
                    font-size: 15px;
                    font-family: arial;
                    text-align: left;
                    padding: 11px;
                    border: 1px solid #80808073;
                    background: #D7D7D7;
                }

                .epdf_spec_title{
                    text-align: left;
                    padding: 10px 10px 10px 25px;
                    font-family: arial;
                    font-weight: 600;
                    color: #5a5a5a;
                    background: #ECECEC;
                    border: 1px solid #dadada;
                }

                .epdf_spec_value{
                    padding: 10px 10px 10px 25px;
                    font-family: arial;
                    font-weight: 400;
                    border: 1px solid #dadada;
                }

                .epdf_table{
                    border-collapse: collapse;
                    border-radius:10px
                }
            </style>

            <div class="epdf_templ_header">
                <div class="epdf_templ_header_info">
                    <h2>Copiersonsale Specsheets PDF</h2> 
                    <span> 20/10/2001 - <a style="text-decoration:none; color:black" href="mailto:info@copiersonsale.com">info@copiersonsale.com</a>  - <a href="tel:18559559855"> +1 855 955 9855 </a></span>
                </div>
                <div class="pdfe_templ_phone_number">
                    <span><a href="tel:18559559855">1 855 955 9855</a></span>
                </div>
            </div>

            <div class="epdf_container">
                <div class="epdf_main_info">
                    <div class="epdf_templ_image_block">
                        <img src="<?php echo (get_the_post_thumbnail_url($id)) ?>">
                        
                    </div>
                    <div class="epdf_templ_main_info">
                        <h1><a style="text-decoration:none; color:black; cursor:pointer" href="<?php echo(get_permalink($id))?>"><?php echo($product->get_name())?></a></h1>
                        <span> <a style="text-decoration: none; color: #C0022B;"href="https://copierosnsale.com">Copieronsale.com</a></span>
                    </div>
                </div>
            </div>


            <div class="epdf_templ_container">
                
                <table class="epdf_table" style="margin-top: 50px ; width: 100%;">
                    <thead>
                    <tr>
                        <th class="epdf_th">GENERAL SPECIFICATIONS</th>
                        <th class="epdf_th" style="text-transform: uppercase"><?php echo($product->get_name())?></th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($items as $taxonomy => $attribute_obj ) :?>
                            <?php 
                                $attribute_label_name = wc_attribute_label($taxonomy);
                                $item_value = $product->get_attribute($attribute_label_name);
                            ?> 
                            <tr>
                                <th class="epdf_spec_title"><?php echo($attribute_label_name)?></th>
                                <td class="epdf_spec_value"><?php echo($item_value)?></td>
                            </tr>
                            <?php endforeach ?>
                    </tbody>
                </table>

            </div>

            <?php

            $mpdf->WriteHTML(ob_get_clean());

            return $mpdf->output('', 'S');
        // If you want use WooCommerce functions, do that after WooCommerce is loaded
        add_action( 'woocommerce_loaded', 'my_function_with_wc_functions' );        
    }
    
    function my_function_with_wc_functions() {
    
        $product = wc_get_product('16');
    
    }
}