<?php

add_action('wp_enqueue_scripts', 'load_modal_enquery');
function load_modal_enquery() {
  wp_enqueue_script('js-Modal', plugin_dir_url( __FILE__ ) . 'js/modal.js', false);
  wp_enqueue_style( 'Modal-css', plugin_dir_url( __FILE__ ) . 'css/modal.css' );
}

add_action( 'wp_footer', 'my_footer_scripts' );
function my_footer_scripts(){
  ?>
  <script>
    if ( window.history.replaceState ) {
            window.history.replaceState( null, null, window.location.href );
        }

    const modal_container = document.getElementById("modal_container")
    function succesModal(email){
        const icon = '<div class="epdf_icon check"> <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-5 -7 60 65" style="enable-background:new 0 0 50 50;" xml:space="preserve"> <circle cx="25" cy="25" r="25"/> <polyline style="fill:none;stroke:#FFFFFF;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;" points=" 38,15 22,33 12,25 "/> </svg> </div>' 
        modal_container.innerHTML = `<div class="epdf_modal"> <button id="epdf_close_modal" class="epdf_close">X</button>${icon}<h3 class="epdf_succes">Email with specs was Send!</h3><span>Please make sure email is on your inbox, if not <span class="epdf_resend">click here</span></span><button id="epdf_close_modal" class="epdf_button">Close Modal</button></div>`
        modal_container.classList.add('show')
        modalevents()
      
    }

    function unSuccesModal(email){
        modal_container.innerHTML = `<div class="epdf_modal"> <button id="epdf_close_modal" class="epdf_close">X</button><h3 class="epdf_succes">Email with specs was Send!</h3><span>Please make sure email is on your inbox, if not <span class="epdf_resend">click here</span></span><button class="epdf_button">Close Modal</button></div>`
        modal_container.classList.add('show')
    }
  </script>
  <?php
}
