
document.addEventListener("DOMContentLoaded", function() {
    const modal_container = document.getElementById("modal_container")
    
    modalevents = () => {
        document.querySelectorAll('#epdf_close_modal').forEach(item => {
            item.addEventListener('click', event => {
                modal_container.classList.remove('show')
            })
          })
    
        document.querySelectorAll('#epdf_open_modal').forEach(item => {
            item.addEventListener('click', event => {
                modal_container.classList.add('show')
            })
          })
    }
    modalevents()
});
