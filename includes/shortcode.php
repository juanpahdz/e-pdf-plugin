<?php

add_shortcode( 'Epdf_send_btn', 'Epdf_send_btn_shortcode');
function Epdf_send_btn_shortcode($atts){

    require EPDF_PLUGIN_DIR . '/includes/controller.php';

    createPDF();

    $atts = shortcode_atts( array(
        'id' => '',
        'sku' => '',
        'class' => '',
        'shortcode' => '',
        'text' => '',
    ), $atts, 'Epdf_send_btn' );

    if ( ! ( ! empty($atts['id']) && $atts['id'] > 0 ) ) {
        if( ! (empty($atts['sku']) ))
       {
           $atts['id'] = wc_get_product_id_by_sku($atts['sku']);
       }
       else {
           $atts['id'] = get_the_id();
       }
   }

    global $wp_session;

    ob_start();
    ?>
    <?php $_POST['epdfemail'] == ""; ?>
    <?php $_POST['epdfproduct_sku'] == ""; ?>
    <?php if($wp_session['FormStatus'] == "success"): ?>
        <script> document.addEventListener("DOMContentLoaded", function() {succesModal()}) </script>
    <?php endif; ?>

    <div id="modal_container" class="epdf_modal_container">

        <div class="epdf_modal">

            <button id="epdf_close_modal" class="epdf_close">X</button>

            <h3>You are one step close to download your pdf</h3>

            <span>Please fill email input and we'll send you a pdf by email</span>
            
            <form class="epdf_download_form"  method="POST" >
                <input class="epdf_input_control" type="email" name="epdfemail" placeholder="hello@example.com" required>
                <input class="epdf_input_control" id="prodId" name="epdfproduct_sku" type="hidden" value="<?php echo $atts['id']?> ">
                <button class="epdf_button">Send Specs By Email</button>
            </form>
        </div>
    </div>
        <button id="epdf_open_modal" class="<?php echo $atts['class'] ?>"><?php echo $atts['text'] ?></button>
    <?php

    return ob_get_clean();
}