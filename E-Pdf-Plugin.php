<?php
/** 
    * Plugin Name: EPdf-Plugin
    * Author: Juan Pablo Hernandez
    * Description: Send Product Sepcs Trought Email using a custom button with the shortcode [Epdf_send_btn]
    * Version: 0.0.1
**/

define( 'EPDF_PLUGIN', __FILE__ );

define( 'EPDF_PLUGIN_DIR', untrailingslashit( dirname( EPDF_PLUGIN ) ) );

define('EPDF_FILE_URL', __FILE__);


require_once  EPDF_PLUGIN_DIR . '/includes/shortcode.php';

require_once EPDF_PLUGIN_DIR . '/includes/modal_enquery.php';


if ( is_readable( __DIR__ . '/vendor/autoload.php' ) ) {
    require __DIR__ . '/vendor/autoload.php';
}

// require_once EPDF_PLUGIN_DIR . '/includes/modal_enquery.php';


// require_once  EPDF_PLUGIN_DIR . '/includes/activation.php';


// require_once EPDF_PLUGIN_DIR . '/admin/admin.php';
